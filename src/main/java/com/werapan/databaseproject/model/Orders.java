/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NonWises
 */
public class Orders {
    private int id;
    private Date date;
    private int qty;
    private double total;
    private ArrayList<OrderDetail> orderDetails;

    public Orders(int id, Date date, int qty, double total, ArrayList<OrderDetail> orderDetails) {
        this.id = id;
        this.date = date;
        this.qty = qty;
        this.total = total;
        this.orderDetails = orderDetails;
    }

    public Orders() {
        this.id = -1;
        orderDetails = new ArrayList<>(); 
        qty = 0;
        total = 0;
        date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(ArrayList<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }
    
    public void addOrderDetail(OrderDetail orderDetail){
        orderDetails.add(orderDetail);
        total = total + orderDetail.getTotal();
        qty = qty+orderDetail.getQty();
    }
    public void addOrderDetail(Product product, String productName, double productPrice, int qty){
        OrderDetail orderDetail = new OrderDetail(product, productName, productPrice, qty, this);
        this.addOrderDetail(orderDetail);
    }
    public void addOrderDetail(Product product, int qty){
        OrderDetail orderDetail = new OrderDetail(product, product.getName(), product.getPrice(), qty, this);
        this.addOrderDetail(orderDetail);
    }
    
    public static Orders fromRS(ResultSet rs) {
        Orders order = new Orders();
        try {
            order.setId(rs.getInt("order_id"));
            order.setQty(rs.getInt("order_qty"));
            order.setTotal(rs.getInt("order_total"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //System.out.println(rs.getString("order_date"));
            order.setDate(sdf.parse(rs.getString("order_date")));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }
   

    @Override
    public String toString() {
        return "Orders{" + "id=" + id + ", date=" + date + ", qty=" + qty + ", total=" + total + '}';
    }
    
    
    
    
    
    
}
